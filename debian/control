Source: translate-toolkit
Section: python
Priority: optional
Uploaders:
 Stuart Prescott <stuart@debian.org>
Maintainer: Debian l10n developers <debian-l10n-devel@lists.alioth.debian.org>
Build-Depends-Indep:
 furo,
 libexttextcat-data,
 python3-all,
 python3-aeidon (>> 1.15~),
 python3-bs4,
 python3-charset-normalizer,
 python3-cheroot,
 python3-cwcwidth,
 python3-diff-match-patch,
 python3-enchant,
 python3-iniparse,
 python3-levenshtein,
 python3-lxml,
 python3-matplotlib,
 python3-mistletoe (>> 1.1.0~),
 python3-phply,
 python3-pyparsing (>> 3),
 python3-pytest,
 python3-ruamel.yaml,
 python3-sphinx-copybutton,
 python3-sphinxext-opengraph,
 python3-syrupy,
 python3-sphinx,
 python3-vobject,
 python3-xapian,
 rename,
 subversion,
Build-Depends:
 debhelper-compat (= 13),
 dh-python (>> 5.20220923~),
 pybuild-plugin-pyproject,
 python3-setuptools,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/l10n-team/translate-toolkit.git
Vcs-Browser: https://salsa.debian.org/l10n-team/translate-toolkit
Homepage: https://toolkit.translatehouse.org/
Rules-Requires-Root: no

Package: translate-toolkit
Architecture: all
Section: devel
Depends:
 python3,
 python3-translate (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends}
Recommends:
 translate-toolkit-doc
Description: Toolkit assisting in the localization of software
 The Translate Toolkit is a Python library and a set of software designed
 to help make the lives of localizers both more productive and less
 frustrating.
 .
 The software includes programs to convert localization formats to the
 common PO format and programs to check and manage PO files and
 utilities to create word counts, merge translations and perform
 various checks on PO files.
 .
 Supported localization storage formats are: DTD, properties,
 OpenOffice.org GSI/SDF, CSV, MO, Qt .ts and of course PO and XLIFF.
 .
 Support for specific features and formats requires various additional
 packages to be installed. These are listed in the Recommends for the
 python3-translate package.
 .
 This package includes documentation for the command line utilities.

Package: python3-translate
Architecture: all
Depends:
 gettext,
 libexttextcat-data,
 python3,
 ${misc:Depends},
 ${python3:Depends}
Recommends:
 python3-aeidon (>> 1.15~),
 python3-bs4,
 python3-charset-normalizer,
 python3-cwcwidth,
 python3-diff-match-patch,
 python3-enchant,
 python3-iniparse,
 python3-levenshtein,
 python3-lxml,
 python3-mistletoe (>> 1.1.0~),
 python3-phply,
 python3-pyparsing (>> 3),
 python3-ruamel.yaml,
 python3-vobject,
 python3-xapian,
Suggests:
 python3-cheroot,
 python3-subversion,
 translate-toolkit-doc
Description: Toolkit assisting in the localization of software (Python 3)
 The Translate Toolkit is a Python library and a set of software designed
 to help make the lives of localizers both more productive and less
 frustrating.
 .
 The software includes programs to convert localization formats to the
 common PO format and programs to check and manage PO files and
 utilities to create word counts, merge translations and perform
 various checks on PO files.
 .
 Supported localization storage formats are: DTD, properties,
 OpenOffice.org GSI/SDF, CSV, MO, Qt .ts and of course PO and XLIFF.
 .
 Support for specific features and formats requires various additional
 packages to be installed. These are listed in the Recommends.
 .
 This package includes the Translate Toolkit modules for Python 3.

Package: translate-toolkit-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: Toolkit assisting in the localization of software (documentation)
 The Translate Toolkit is a Python library and a set of software designed
 to help make the lives of localizers both more productive and less
 frustrating.
 .
 The software includes programs to convert localization formats to the
 common PO format and programs to check and manage PO files and
 utilities to create word counts, merge translations and perform
 various checks on PO files.
 .
 Supported localization storage formats are: DTD, properties,
 OpenOffice.org GSI/SDF, CSV, MO, Qt .ts and of course PO and XLIFF.
 .
 This package includes the Translate Toolkit API documentation.
